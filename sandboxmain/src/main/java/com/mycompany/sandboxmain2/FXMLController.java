package com.mycompany.sandboxmain2;

import com.mycompany.sandboxsub.SandboxSubmodule;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
	 private SandboxSubmodule submodule = new SandboxSubmodule();
	 
	 
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me! "  + submodule.getVersion());
        label.setText("Submodule vesion: " + submodule.getVersion());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
